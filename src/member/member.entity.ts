import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export interface NewMember {
  firstName: string;
  lastName: string;
  email: string;
  cc: string;
  id?: number;
  referredFrom?: number;
  referralCount?: number;
}

@Entity()
export class Member {
  constructor(
    id: number,
    firstName: string,
    cc: string,
    lastName: string,
    email: string,
  ) {
    this.id = id;
    this.firstName = firstName;
    this.cc = cc;
    this.lastName = lastName;
    this.email = email;
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  firstName: string;

  @Column('varchar')
  cc: string;

  @Column('varchar')
  lastName: string;

  @Column({default: 0})
  referralCount: number;

  @Column({default: null})
  referredFrom: number;

  @Column('varchar')
  email: string;
}
