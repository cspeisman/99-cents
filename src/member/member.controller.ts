import {Body, Controller, Get, Post, Query, Res} from '@nestjs/common';
import {MemberService} from './member.service';
import {NewMember} from './member.entity';

@Controller('/members')
export class MemberController {
  constructor(private memberService: MemberService) {}

  @Get()
  async getMembers(@Query('userId') id = null, @Res() res) {
    const allMembers = await this.memberService.getMembers();
    const userId = Number(id);
    res.render('members/index', {userId, allMembers});
  }

  @Post('/create')
  async createMember(@Body() member: NewMember, @Res() res, @Query('referralId') referralId = null) {
    let createdMember;
    if (referralId) {
     createdMember = await this.memberService.createAndIncrementReferralCount(referralId, member);
    } else {
      createdMember = await this.memberService.createMember(member);
    }
    res.redirect(`/members?userId=${createdMember.id}`);
  }
}
