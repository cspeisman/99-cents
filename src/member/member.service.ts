import { Member, NewMember } from './member.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private memberRepo: Repository<Member>,
  ) {}

  async createMember(member: NewMember): Promise<Member> {
    return await this.memberRepo.save(member);
  }

  async getMembers(): Promise<Member[]> {
    return await this.memberRepo.find();
  }

  async createAndIncrementReferralCount(referralId: number, nm: NewMember) {
    const member = await this.memberRepo.findOne(referralId);
    member.referralCount++;
    this.memberRepo.save(member);
    nm.referredFrom = member.id;
    return await this.createMember(nm);
  }
}
