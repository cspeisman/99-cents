import { MemberController } from '../member.controller';
import { TestingModule } from '@nestjs/testing';
import { createTestModule, mockRepository } from './mocks';

describe('MemberController', () => {
  let app: TestingModule;
  let memberController: MemberController;

  beforeAll(async () => {
    app = await createTestModule();
    memberController = app.get<MemberController>(MemberController);
  });

  afterEach(() => mockRepository.clean());
  describe('create', () => {
    const member = {
      firstName: 'Corey',
      lastName: 'Speisman',
      email: 'cspeisman@example.com',
      cc: '4242424242424242',
    };
    const res = {redirect: jest.fn()};

    it('should take member information and save a new member', async () => {
      await memberController.createMember(member, res);
      const createdMember = mockRepository.findOne(member);
      expect(res.redirect).toBeCalledWith(`/members?userId=${createdMember.id}`);
    });

    it('should create a member from a referral and increment the referral count for the referee', async () => {
      const newMember = {
        firstName: 'referralFirst',
        lastName: 'referralLast',
        email: 'first@example.com',
        cc: '',
      };

      let existingMember = await mockRepository.save(newMember);
      await memberController.createMember(member, res, existingMember.id);
      existingMember = mockRepository.findOne(existingMember);
      const createdMember = mockRepository.findOne(member);
      expect(existingMember.referralCount).toBe(1);
      expect(createdMember.referredFrom).toBe(1);
    });
  });

  it('should return render a list of all the members', async () => {
    const res = { render: jest.fn() };
    await memberController.getMembers(0, res);
    const allMembers = mockRepository.find();
    allMembers[0].isYou = true;
    expect(res.render).toBeCalledWith('members/index', {allMembers, userId: 0});
  });
});
