import { Member, NewMember } from '../member.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Test } from '@nestjs/testing';
import { MemberController } from '../member.controller';
import { MemberService } from '../member.service';

export const mockRepository = {
  members: [
    new Member(0, 'first', '1234123412341234', 'last', 'flast@example.com'),
  ],

  save(member: NewMember | Member) {
    if (member.id) {
      this.members[member.id] = member;
    } else {
      const createdMember = { ...member, id: this.members.length, referralCount: 0 }
      this.members.push(createdMember);
      return Promise.resolve(createdMember);
    }
  },

  findOne(nm: NewMember | number): Member {
    if (typeof nm === 'number') {
      return this.members.find(m => m.id === nm);
    } else {
      return this.members.find(
        member =>
          member.firstName === nm.firstName && member.lastName === nm.lastName,
      );

    }
  },

  find() {
    return this.members;
  },

  clean() {
    this.members = [
      new Member(0, 'first', '1234123412341234', 'last', 'flast@example.com'),
    ];
  },
};

export const mockRepoProvider = {
  provide: getRepositoryToken(Member),
  useValue: mockRepository,
};

export const createTestModule = async () => {
  return await Test.createTestingModule({
    controllers: [MemberController],
    providers: [MemberService, mockRepoProvider],
  }).compile();
};
