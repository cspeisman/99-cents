import { MemberService } from '../member.service';
import { createTestModule, mockRepository } from './mocks';
import { NewMember } from '../member.entity';
import { TestingModule } from '@nestjs/testing';

describe('MemeberService', () => {
  let app: TestingModule;
  let memberService: MemberService;

  beforeAll(async () => {
    app = await createTestModule();
    memberService = app.get<MemberService>(MemberService);
  });

  afterEach(() => mockRepository.clean());

  it('should create a new member', async () => {
    const newMember: NewMember = {
      firstName: 'Corey',
      lastName: 'Speisman',
      cc: '424242424242',
      email: 'cspeisman@example.com',
    };
    const createdMember = await memberService.createMember(newMember);
    expect(createdMember.id).not.toBeNull();
    expect(mockRepository.findOne(createdMember)).toBe(createdMember);
  });

  it('should return get all members', async () => {
    const members = await memberService.getMembers();
    expect(members.length).toBe(1);
  });
});
