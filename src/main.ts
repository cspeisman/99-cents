import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { renderFile } from 'ejs';
import {join} from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.set('views', 'views');
  app.set('view engine', 'ejs')
    .useStaticAssets(join(__dirname, '../public'));

  await app.listen(process.env.PORT || 8080);
}
bootstrap();
