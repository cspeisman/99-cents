import {Controller, Get, Query, Res} from '@nestjs/common';

@Controller()
export class AppController {

  @Get('/')
  index(@Res() res, @Query('referralId') id = null): void {
    res.render('index', {referralId: id});
  }
}
