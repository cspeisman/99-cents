import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AppController} from './app.controller';
import {MemberModule} from './member/member.module';

const db = process.env.TEST ? '99-cents-test' : '99-cents';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      database: `${db}`,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    MemberModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
