import { AppController } from '../app.controller';

describe('AppController', () => {
  let appController: AppController;

  describe('root', () => {
    appController = new AppController();
    it('should render the landing page', () => {
      const res = { render: jest.fn() };
      appController.index(res);
      expect(res.render).toBeCalledWith('index', {referralId: null});
    });

    it('should add a referralId from the url', () => {
      const res = { render: jest.fn() };
      appController.index(res, 1);
      expect(res.render).toBeCalledWith('index', {referralId: 1});
    });
  });
});
