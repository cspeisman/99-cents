describe('Landing Page', () => {
  it('should fill out the form and navigate to the members page', () => {
    cy.visit('http://localhost:8080')
      .get('input[name="firstName"]')
      .type('First1')
      .get('input[name="lastName"]')
      .type('Last1')
      .get('input[name="email"]')
      .type('email1@example.com')
      .get('input[name="cc"]')
      .type('4242424242424242');

    cy.get('form').submit();
    cy.contains('members');
    cy.contains('First1 Last1 That\'s you!');

    cy.visit('http://localhost:8080')
      .get('input[name="firstName"]')
      .type('First2')
      .get('input[name="lastName"]')
      .type('Last2')
      .get('input[name="email"]')
      .type('email2@example.com')
      .get('input[name="cc"]')
      .type('111111111111111');

    cy.get('form').submit();
    cy.contains('members');
    cy.contains('First1 Last1');
    cy.contains('First2 Last2');
  });
});
