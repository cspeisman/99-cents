### Welcome to my 99 cents app

---
#### installing

requirements

    - node
    - postgres
    
__step 1.__ clone down repository

__step 2.__ `cd 99-cents`

__step 3.__ run `npm install` or `yarn`

__step 4.__ setup your database by running `./db-setup.sh` (if the correct permissions aren't set please `chmod u+x db-setup.sh` and try to run again)

__step 5.__ run the app by `npm run start` or `yarn start`

__step 6.__ visit [http://localhost:8080](http://localhost:8080)


#### Running tests

This codebase consists of unit, integration and journey tests. In order to run all the tests at once please run `npm run all-tests` or `yarn all-test`

#### About this project

This project is built in nestjs with typeorm and ejs templates. I chose nestjs to explore my interest in typescript. One of the selling points to me of nestjs is their module architecture which uses dependency injection similar to angular's DI. This allows me to abstract layers of my codebase into an interface, such as `MemberRepository`. This allows me create a MemberRepository implemenataion that talks to the database and one that is stubbed out. Which ultimately allows for quicker testing and more isolated tests while I can stay confident that both repostories are adhearing to the same interface.

I originally was going to use reactjs for the frontend but after exploring the mockups I decided that adding a framework could be fairly heavy handed for 2 views. Instead I went with ejs and rendering my templates on the server.

#### Things left to do

No software is ever done and that is especially the case with this codebase. In order to complete this in a timely manner I had to prioritize which means a handful of things got deprioritzed. If given more time I would handle:

 - Finish styling: A lot of the styles on the members page didn't get implemented. Such as the referral share links. For me it is more important that they are functional and can get put infront of users to test our hypothesis around referrals rather than looking pixel perfect.
 Similarly for the credit card input.
 -  Form validation: Unfortunately although the UI says certain fields are required there is no real form validation and the form will submit with any values in its input.
 - explore heap analytics more: I chose heap analytics as my analytics platform without ever using it before. I feel like I got the barebone functionality out of it but I would love to do a deeper exploration for the platform in order to best support my expirements
 - more type safety: I used typescript because I love compile time errors over runtime errors. If given more time I would try to add more types and try to cash in the confidence gained from having a type safe codebase
 
