#!/usr/bin/env bash

echo "attempting to delete an existing database"
echo "---------------------------------------------"
dropdb 99-cents
dropdb 99-cents-test

echo "creating database 99-cents and 99-cents-test"
echo "---------------------------------------------"
createdb 99-cents
createdb 99-cents-test
