#!/bin/sh

function checkServer() {
  for value in {1..5}
  do
    curl http://localhost:8080
    if [[ $? -eq 0 ]]
    then
      return 0
    fi
    sleep 2
  done
    return 1
}

yarn test
TEST=true yarn start >/dev/null &
BACKEND_PID=$!

checkServer && yarn journey || echo "uh oh"

kill $BACKEND_PID
